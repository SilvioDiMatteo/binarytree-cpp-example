#include <iostream>
#include "BinaryTree.h"

int main()
{
	using namespace BinTree;
	
	BinaryTree binaryTree;

	//for (int i = 0; i < 100; i++)
	//{
	//	int randomNumber = rand() % 101;

	//	binaryTree.AddElement(randomNumber);

	//	/*BinaryTreeNode* newNode = new BinaryTreeNode(randomNumber);

	//	if (binaryTree.Insert(newNode))
	//	{
	//		std::cout << "Generated number: " << randomNumber << std::endl;
	//	}
	//	else
	//	{
	//		delete newNode;
	//	}*/
	//}

	/*int dataToCheck = 6;

	binaryTree += dataToCheck;
	std::cout << "Does the tree contain the value " << dataToCheck << "? " << binaryTree.Contains(dataToCheck) << std::endl;
	binaryTree -= dataToCheck;
	std::cout << "Does the tree contain the value " << dataToCheck << "? " << binaryTree.Contains(dataToCheck) << std::endl;
*/
	
	binaryTree += { 1, 2, 3, 4, 5 };

	for (int i = 0; i < 5; i++) 
	{
		std::cout << "Does the tree contain the value " << i + 1 << "? " << binaryTree.Contains(i + 1) << std::endl;
	}

	binaryTree -= {1, 4};

	for (int i = 0; i < 5; i++)
	{
		std::cout << "Does the tree contain the value " << i + 1 << "? " << binaryTree.Contains(i + 1) << std::endl;
	}

	system("PAUSE");
	return 0;
}