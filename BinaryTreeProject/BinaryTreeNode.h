#pragma once

namespace BinTree
{
	class BinaryTreeNode
	{
	private:
		// The direct parent of this node (Is NULL if this node is the root of the tree)
		BinaryTreeNode* m_Parent;
	public:
		// The left child of the node
		BinaryTreeNode* m_Left;
		// The right child of the node
		BinaryTreeNode* m_Right;

		// The data contained by this node
		int m_Data;

		BinaryTreeNode(int data);
		~BinaryTreeNode();

		// Sets the node parent
		void SetParent(BinaryTreeNode* node);
		// Removes this node from the tree
		void Remove();
	};
}