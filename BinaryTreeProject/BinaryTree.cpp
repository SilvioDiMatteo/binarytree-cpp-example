#include <iostream>
#include "BinaryTree.h"

namespace BinTree
{
	BinaryTree::~BinaryTree()
	{
		if (m_Root == nullptr) { return; }
		delete m_Root;
	}

	bool BinaryTree::Insert(BinaryTreeNode* node)
	{
		if (node == nullptr) { std::cout << "The TreeNode to insert can't be null" << std::endl; }
		if (m_Root == nullptr)
		{
			m_Root = node;
			return true;
		}

		bool inserted = false;
		BinaryTreeNode* target = m_Root;
		while (!inserted)
		{
			if (node->m_Data > target->m_Data)
			{
				if (target->m_Right != nullptr)
				{
					target = target->m_Right;
					continue;
				}
				else
				{
					target->m_Right = node;
					target->m_Right->SetParent(target);
					inserted = true;
				}
			}
			else if (node->m_Data < target->m_Data)
			{
				if (target->m_Left != nullptr)
				{
					target = target->m_Left;
					continue;
				}
				else
				{
					target->m_Left = node;
					target->m_Left->SetParent(target);
					inserted = true;
				}
			}
			else
			{
				break;
			}
		}
		return inserted;
	}

	void BinaryTree::AddElement(int element)
	{
		BinaryTreeNode* node = new BinaryTreeNode(element);

		if (!Insert(node))
		{
			delete node;
		}
	}

	BinaryTreeNode* BinaryTree::Search(int element) const
	{
		if (m_Root == nullptr)
		{
			return nullptr;
		}
		BinaryTreeNode* target = m_Root;

		while (target != nullptr)
		{
			if (element > target->m_Data)
			{
				target = target->m_Right;
			}
			else if (element < target->m_Data)
			{
				target = target->m_Left;
			}
			else
			{
				return target;
			}
		}
		return nullptr;
	}

	void BinaryTree::RemoveElement(int element)
	{
		if (m_Root == nullptr) { return; }
		BinaryTreeNode* target = Search(element);
		if (target == nullptr) { std::cout << "There isn't a node to remove, with the data " << element << std::endl; }

		if (target == m_Root) 
		{
			m_Root = nullptr;
		}
		else 
		{
			target->Remove();
		}

		if (target->m_Left != nullptr)
		{
			Insert(target->m_Left);
			target->m_Left = nullptr;
		}
		if (target->m_Right != nullptr)
		{
			Insert(target->m_Right);
			target->m_Right = nullptr;
		}
		delete target;
	}

	bool BinaryTree::Contains(int data) const
	{
		return Search(data) != nullptr;
	}

	BinaryTree& BinaryTree::operator+=(int element) 
	{ 
		AddElement(element); 
		return *this;
	}

	BinaryTree& BinaryTree::operator+=(const std::vector<int>& elements)
	{
		for (int element : elements)
		{
			AddElement(element);
		}
		return *this;
	}

	BinaryTree& BinaryTree::operator-=(int element)
	{
		RemoveElement(element);
		return *this;
	}

	BinaryTree& BinaryTree::operator-=(const std::vector<int>& elements)
	{
		for (int element : elements)
		{
			RemoveElement(element);
		}
		return *this;
	}
}

