#include "BinaryTreeNode.h"
#include <iostream>

namespace BinTree
{
	BinaryTreeNode::BinaryTreeNode(int data)
		: m_Parent(nullptr), m_Left(nullptr), m_Right(nullptr), m_Data(data) {}

	BinaryTreeNode::~BinaryTreeNode() 
	{
		std::cout << "Deleting node: " << m_Data << std::endl;
		if (m_Left != nullptr)
		{
			delete m_Left;
		}
		if (m_Right != nullptr)
		{
			delete m_Right;
		}
	}

	void BinaryTreeNode::SetParent(BinaryTreeNode* node)
	{
		m_Parent = node;
	}

	void BinaryTreeNode::Remove()
	{
		if (m_Parent == nullptr) { return; }
		if (m_Parent->m_Right != nullptr && m_Parent->m_Right == this)
		{
			m_Parent->m_Right = nullptr;
			return;
		}
		if (m_Parent->m_Left == nullptr) { return; }
		m_Parent->m_Left = nullptr;
	}
}