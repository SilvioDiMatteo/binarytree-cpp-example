#pragma once

#include <vector>
#include "BinaryTreeNode.h"

namespace BinTree
{
	class BinaryTree
	{
	private:
		// The tree root
		BinaryTreeNode* m_Root;
	private:
		// Inserts a new node in the tree	
		bool Insert(BinaryTreeNode* node);
	public:
		BinaryTree() : m_Root(nullptr) {}
		BinaryTree(BinaryTreeNode* root) : m_Root(root) {}
		~BinaryTree();

		// Adds an element to this tree
		void AddElement(int element);
		// Removes a node from the tree
		void RemoveElement(int element);
		// Searchs for a tree node
		BinaryTreeNode* Search(int element) const;
		// Checks if the tree contains a node with a certain data in it
		bool Contains(int element) const;

		// Operators overload
		BinaryTree& operator+=(int element);
		BinaryTree& operator+=(const std::vector<int>& elements);
		BinaryTree& operator-=(int element);
		BinaryTree& operator-=(const std::vector<int>& elements);
	};	
}
